import React, {useState} from 'react'
import {
  BrowserRouter as Router,
  Switch,
  Route,
} from 'react-router-dom';
import List from './views/List'
import Detail from './views/Detail'

const App = (props) => {
  const [characterSelected, setCharacterSelected] = useState({})
  return (
    <Router>
       <Switch>
          <Route path="/detail">
            <Detail characterSelected={characterSelected} />
          </Route>
          <Route path="/">
            <List setCharacterSelected={setCharacterSelected} />
          </Route>  
        </Switch>
    </Router>
  )
}

export default App

