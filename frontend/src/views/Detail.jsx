import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import { useHistory } from "react-router-dom";

const useStyles = makeStyles({
  root: {
    minWidth: 275,
  },
  title: {
    fontSize: 14,
  },
  pos: {
    marginBottom: 12,
  },
});
const Detail = (props) => {
  const classes = useStyles();
  const bull = <span className={classes.bullet}>•</span>;
  const {
    characterSelected
  } = props
  const history = useHistory();

  console.log(characterSelected);
  
  const goBack = () => {
    history.push('/')
  }
  
  const {
    // titles,
    spouse,
    children:childrens,
    // allegiance,
    // books,
    // longevity,
    // longevityB,
    // longevityC,
    // _id,
    // plod,
    // plodB,
    // plodC,
    name,
    // slug,
    gender,
    image,
    father,
    mother,
    // culture,
    house,
    // placeOfBirth,
    birth,
    // placeOfDeath,
    death,
    // alive,
  } = characterSelected
  return (
    <Card className={classes.root}>
      <Button onClick={goBack}>
        Atras
      </Button>
      <CardContent>
        {name && 
        <>
          <Typography className={classes.title} color="textSecondary" gutterBottom>
            Nombre
          </Typography>
          <Typography variant="h5" component="h2">
            {name}
          </Typography>
        </>}
        {house && 
        <>
          <Typography className={classes.title} color="textSecondary" gutterBottom>
            Casa
          </Typography>
          <Typography variant="h5" component="h2">
            {house}
          </Typography>
        </>}
        {gender && 
        <>
          <Typography className={classes.title} color="textSecondary" gutterBottom>
            Genero
          </Typography>
          <Typography variant="h5" component="h2">
            {gender}
          </Typography>
        </>}
        {!Number.isNaN(death-birth) && 
        <>
          <Typography className={classes.title} color="textSecondary" gutterBottom>
            Edad
          </Typography>
          <Typography variant="h5" component="h2">
            {death-birth}
          </Typography>
        </>}
        {image && 
        <>
          <Typography className={classes.title} color="textSecondary" gutterBottom>
            Imagen
          </Typography>
          <img src={image}/>
        </>}
        {father && 
        <>
          <Typography className={classes.title} color="textSecondary" gutterBottom>
            Padre
          </Typography>
          <Typography variant="h5" component="h2">
            {father}
          </Typography>
        </>}
        {mother && 
        <>
          <Typography className={classes.title} color="textSecondary" gutterBottom>
            Madre
          </Typography>
          <Typography variant="h5" component="h2">
            {mother}
          </Typography>
        </>}
        {childrens && childrens.length && 
        <>
          <Typography className={classes.title} color="textSecondary" gutterBottom>
            Madre
          </Typography>
          <Typography variant="h5" component="h2">
            {childrens.join(', ')}
          </Typography>
        </>}
        {spouse && spouse.length && 
        <>
          <Typography className={classes.title} color="textSecondary" gutterBottom>
            Pareja
          </Typography>
          <Typography variant="h5" component="h2">
            {spouse.join(', ')}
          </Typography>
        </>}
        
      </CardContent>
    </Card>
  );
}
export default Detail
