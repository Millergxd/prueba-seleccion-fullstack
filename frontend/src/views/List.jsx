import React, { useEffect, useState, forwardRef, useRef} from 'react';
import MaterialTable from 'material-table';
import request from '../api/api'
import AddBox from '@material-ui/icons/AddBox';
import ArrowDownward from '@material-ui/icons/ArrowDownward';
import Check from '@material-ui/icons/Check';
import ChevronLeft from '@material-ui/icons/ChevronLeft';
import ChevronRight from '@material-ui/icons/ChevronRight';
import Clear from '@material-ui/icons/Clear';
import DeleteOutline from '@material-ui/icons/DeleteOutline';
import Edit from '@material-ui/icons/Edit';
import FilterList from '@material-ui/icons/FilterList';
import FirstPage from '@material-ui/icons/FirstPage';
import LastPage from '@material-ui/icons/LastPage';
import Remove from '@material-ui/icons/Remove';
import SaveAlt from '@material-ui/icons/SaveAlt';
import Search from '@material-ui/icons/Search';
import ViewColumn from '@material-ui/icons/ViewColumn';
import { useHistory } from "react-router-dom";

const tableIcons = {
  Add: forwardRef((props, ref) => <AddBox {...props} ref={ref} />),
  Check: forwardRef((props, ref) => <Check {...props} ref={ref} />),
  Clear: forwardRef((props, ref) => <Clear {...props} ref={ref} />),
  Delete: forwardRef((props, ref) => <DeleteOutline {...props} ref={ref} />),
  DetailPanel: forwardRef((props, ref) => <ChevronRight {...props} ref={ref} />),
  Edit: forwardRef((props, ref) => <Edit {...props} ref={ref} />),
  Export: forwardRef((props, ref) => <SaveAlt {...props} ref={ref} />),
  Filter: forwardRef((props, ref) => <FilterList {...props} ref={ref} />),
  FirstPage: forwardRef((props, ref) => <FirstPage {...props} ref={ref} />),
  LastPage: forwardRef((props, ref) => <LastPage {...props} ref={ref} />),
  NextPage: forwardRef((props, ref) => <ChevronRight {...props} ref={ref} />),
  PreviousPage: forwardRef((props, ref) => <ChevronLeft {...props} ref={ref} />),
  ResetSearch: forwardRef((props, ref) => <Clear {...props} ref={ref} />),
  Search: forwardRef((props, ref) => <Search {...props} ref={ref} />),
  SortArrow: forwardRef((props, ref) => <ArrowDownward {...props} ref={ref} />),
  ThirdStateCheck: forwardRef((props, ref) => <Remove {...props} ref={ref} />),
  ViewColumn: forwardRef((props, ref) => <ViewColumn {...props} ref={ref} />)
};


const List = (props) => {
  const {
    setCharacterSelected
  } = props

  const history = useHistory()
  
  const tableRef = useRef()

  const selectCharacter = (event,rowData) => {
    setCharacterSelected(rowData)
    history.push("/detail");
  }

  const columns =[
    { title: 'Nombre', field: 'name' },
    { title: 'Casa', field: 'house' },
    { title: 'Genero', field: 'gender' },
    { title: 'Edad', field: 'age' },
  ]

  return (
    <MaterialTable
      title="Personajes de GOT"
      columns={columns}
      tableRef= {tableRef}
      options={{
        pageSize:10,
        icons:tableIcons,
        pageSizeOptions:[],

      }}
      icons={tableIcons}
      showFirstLastPageButtons={true}
      onRowClick={selectCharacter}
      data={(query) =>
        new Promise((resolve, reject) => {
          console.log(query);
          
          request({
            method:'GET',
            path: `/characters/${query.page}`
          })
          .then((response) => {
            const { 
              message,
              currentPage,
              numberOfDocs 
            } = response.data
            resolve({
              data: 
              message.map(character => ({
                age: character.death-character.birth || 'No definido',
                ...character
              })),
              page: +currentPage-1,
              totalCount: numberOfDocs
            })
          })
      })}
      actions={[
        {
          icon: 'refresh',
          tooltip: 'Refresh Data',
          isFreeAction: true,
          onClick: () => tableRef.current && tableRef.current.onQueryChange(),
        }
      ]}
      debounceInterval={500}
    />
  );
}

export default List