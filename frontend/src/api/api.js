import axios from 'axios'

const { REACT_APP_BACKEND } = process.env

const request = (params) => {
  const {
    path,
    method,
    ...other
  }= params
  console.log({
    headers:{
      'Content-Type': 'application/json'
    },
    url: `${REACT_APP_BACKEND}${path}`,
    method,
    ...other
  });
  
  return axios({
    headers:{
      'Content-Type': 'application/json'
    },
    url: `${REACT_APP_BACKEND}${path}`,
    method,
    ...other
  })
}

export default request