const Characters = require('../models/character')

exports.findAllCharacters = async (req, res) => {
  try {
    res.send({
      message: await Characters.find({})
    })
  } catch (e) {
    console.log(e);
    res.send({
      error: e
    })
  }
}

exports.findPageOfCharacters = async (req, res) => {
  try {
    const resPerPage = 10;
    const { 
      params:{
        page
      }
    } = req
    const p = +page+1
    const count = await Characters.count({})
    res.send({
      message: await Characters.find({})
      .skip((resPerPage * p) - resPerPage)
      .limit(resPerPage),
      currentPage: p,
      pages: Math.ceil(count/resPerPage),
      numberOfDocs: count
    })
  } catch (e) {
    console.log(e);
    res.send({
      error: e
    })
  }
}

exports.findCharacterById = async (req, res) => {
  try {
    const {
      params: {
        id
      }
    } = req

    res.send({
      message: await Characters.findById({
        _id: id
      })
    })

  } catch (e) {
    console.log(e);
    res.send({
      error: e
    })
  }
}
