const express = require('express');
const app = express();
const morgan = require('morgan');
const cors = require('cors');
const characters = require('./router')
const connectDB = require('./database/mongo')

connectDB()
app.use(cors());

const {
  PORT,
  NODE_ENV
} = process.env

if(NODE_ENV==='dev'){
  app.use(morgan('dev'));
}else{
  app.use(morgan(
    (tokens, req, res) => [
      `[${tokens.date(req, res, 'clf')}]`,
      tokens.method(req, res),
      tokens.url(req, res),
      tokens.status(req, res),
      `${tokens['response-time'](req, res)}ms`,
      tokens['user-agent'](req, res),
    ].join(' '),
  ));
}

app.get('/health', function (req, res) {
  res.send({status: 'ok'});
});

app.listen(PORT || 3000, function () {
  console.log(`App listening on port ${PORT}`);
});

app.use('/characters',characters);

app.get('*', function (req, res) {
  res.send('The route cannot reached');
});