const { ObjectId } = require('mongoose').Types;

exports.verifyId = (req,res, next) => {
  const {
    params:{
      id
    }
  } = req
  
  if(ObjectId.isValid(id)){
    next()
  }else{
    res.status(400).send({message:'id requested its not valid'})
  }
}

exports.verifyPage = (req,res,next) => {
  const {
    params:{
      page
    }
  } = req

  if(page>=0){
    next()
  }else{
    res.status(400).send({message:'page must be bigger than 0'})
  }
}