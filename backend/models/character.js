const mongoose = require('mongoose');

const { Schema } = mongoose;

const PageRank = new Schema({
  titles:{
    type: String
  },
  rank:{
    type: Number
  }
},{
  _id: false
})

// Main schema
const CharactersSchema = new Schema({
  titles:{
    type: Array
  },
  spouse:{
    type: Array
  },
  children:{
    type: Array
  },
  allegiance:{
    type: Array
  },
  books:{
    type: Array
  },
  plod:{
    type: Number
  },
  longevity:{
    type: Array
  },
  plodB:{
    type: Number
  },
  plodC:{
    type: Number
  },
  longevityB:{
    type: Array
  },
  longevityC:{
    type: Array
  },
  name:{
    type: String
  },
  slug:{
    type: String
  },
  gender:{
    type: String
  },
  image:{
    type: String
  },
  culture:{
    type: String
  },
  house:{
    type: String
  },
  alive:{
    type: Boolean
  },
  pagerank:{
    type: PageRank
  }
}, {
  versionKey: false,
});

module.exports = mongoose.model('characters', CharactersSchema);
