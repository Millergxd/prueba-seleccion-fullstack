const mongoose = require('mongoose');

const {
  MONGO_URI,
  MONGO_USERNAME,
  MONGO_PASSWORD,
} = process.env;

/* eslint-disable */
const connectDB = async () => {
  try {
    const connection = await mongoose.connect(MONGO_URI, {
      user: MONGO_USERNAME,
      pass: MONGO_PASSWORD,
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useCreateIndex: true,
      useFindAndModify: false,
    });

    console.log(`MongoDB Connected: ${connection.connection.host}`);
    return connection;
  } catch (err) {
    console.log(err.stack);
    process.exit(1);
  }
};
/* eslint-enable */

module.exports = connectDB;
