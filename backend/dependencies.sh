#!/bin/bash
echo $NODE_ENV
if [ $NODE_ENV == "prd" ] || [ $NODE_ENV == "qa" ]; then 
  npm i --only=production
  npm i pm2 -g
else 
  npm i
fi