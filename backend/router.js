
const express = require('express');
const router = express.Router();
const { 
  verifyId,
  verifyPage
} = require('./middlewares/characters')
const { 
  findAllCharacters,
  findCharacterById,
  findPageOfCharacters
} = require('./controllers/characters')

router.route('/').get(findAllCharacters);

router.route('/:page').get(verifyPage,findPageOfCharacters);
router.route('/:id').get(verifyId, findCharacterById);

module.exports = router;